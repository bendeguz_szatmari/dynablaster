package hu.bme.mit.szofttech.dynablaster;

import java.io.*;
import java.net.*;

import javax.swing.JOptionPane;

public class SerialClient extends Network {

	private Socket socket = null;
	private ObjectOutputStream out = null;
	private ObjectInputStream in = null;
	static boolean SERVER = false;//van kapcsolódott szerver és elküldtük neki a szinkronizációs tömböt

	SerialClient(Control c) {
		super(c);
	}

	private class ReceiverThread implements Runnable {

		public synchronized void run() {
			try {
				while(true){
					//this.wait();
					//ctrl.receiveData(in);
					//this.notifyAll();
					//for(int i = 0;i<10;i++){
						ctrl.receiveData(in);
						//this.wait(200);
						SERVER = true;
					//}

				}
			} 
			catch (Exception ex) {
				System.out.println(ex.getMessage());
				System.err.println("Server disconnected!");
			} finally {
				disconnect();
			}
		}
	}

	@Override
	void connect(String ip) {
		disconnect();
		try {
			socket = new Socket(ip, 10007);

			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			out.flush();

			Thread rec = new Thread(new ReceiverThread());
			rec.start();
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host");
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection. ");
			JOptionPane.showMessageDialog(null, "Cannot connect to server!");
			System.exit(1);
		}
	}

	void send(FieldObject[] fo) {
		if (out == null)
			return;
		System.out.println("Sending data to Server");
		try {
			out.writeObject(fo);
			out.flush();
		} catch (IOException ex) {
			System.err.println("Send error.");
		}
	}
	
	@Override
	void disconnect() {
		try {
			if (out != null)
				out.close();
			if (in != null)
				in.close();
			if (socket != null)
				socket.close();
		} catch (IOException ex) {
			System.err.println("Error while closing conn.");
		}
	}
}
