package hu.bme.mit.szofttech.dynablaster;

import javax.swing.JPanel;

import java.awt.*;
import java.awt.image.*;
import java.awt.Point;
import java.util.Random;


//a játék főpaneljes
public class GamePanel extends JPanel implements Runnable {
	
	private static final long serialVersionUID = 3L; //sorosítási azonosító
	public static final int width = 450; //pályaméret
	public static final int height = 450;
	private Thread thread;
	private boolean running;
	private BufferedImage image;
	private Graphics2D g;
	private int FPS = 3;
	private int targetTime = 1000/FPS;
	
	private TileMap tileMap;
	static Player[] player = new Player[2];
	static Monster []mArray = new Monster[3];
	Wall[] wArray = new Wall[50];
	static FieldObject[] syncArray = new FieldObject[59];

	private int updatehelper=0;
	private int x;
	private int y;
	
	static int GPID; //pályaazonosító, server GPID: 0, client GPID: 1,2,...
	
	public static Point[] BombPoints = new Point[4]; //bombák helyének jelölése, későbbi munlkálatokhoz
	//public Control ctrl;
	
	public GamePanel(int gpid){
		super();
		setPreferredSize(new Dimension( width, height));
		this.GPID = gpid;
	}	
	
	
	public void addNotify(){
		super.addNotify();
		if (thread == null){
			thread = new Thread(this);
			thread.start();
		}
	}
		public synchronized void run (){
			
			init();
		
			long startTime;
			long urdTime;  
			long waitTime;
			
			while (running){
				

				startTime= System.nanoTime();
				
				if(SerialServer.KLIENS || SerialClient.SERVER) {
					update();
					render();
					draw();
				}
				sync();
		
				urdTime = ( System.nanoTime()-startTime )/10000000;
				waitTime= targetTime- urdTime;
				/*this.notifyAll();
				try {
					this.wait();
					System.out.println("GP szál várakozik: " + this.thread.getState());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
		}
		//inicializáló függvény
		private void init(){
			running = true;
			image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			MotionAction ma = new MotionAction(this); 
			ma.addMotionSupport(); //mozgás támogatás hozzáadása
			g = (Graphics2D) image.getGraphics();
			tileMap= new TileMap("resources/testmap.txt",30); //pályarajzolás
			if(GPID == 0) initwall();			//falak létrehozása
			tileMap.draw(g);
			System.out.println("GPID: " + GPID);
			//játékos kirajzolása, szerver -> player1, kliens -> player1
			player[0] = new Player(0, FieldObject.P0, "Player1",3);
			player[1] = new Player(1, FieldObject.P1, "Player2",3);
			System.out.println("Játékosok létrehozva");
			if(GPID == 0){
				this.x = player[0].getP().x;
				this.y = player[0].getP().y;
				for(int i=0; i<3;i++){
					mArray[i]= new Monster(20+i, randmonsterXY(), "Monster"+i,1);
				}
				System.out.println("Szörnyek létrehozva");
			}
			else{
				this.x = player[1].getP().x;
				this.y = player[1].getP().y;
			}
		}
		//adatfrissítő függvény
		private void update()	{	
			if(GPID == 0){
				// if player0 dies, respawn at (1,1)
				if(player[0].respawn == true){
					player[0].respawn = false;
					player[0].update(new Point(1,1));
					this.setX(1);
					this.setY(1);
				}
				else player[0].update(new Point(this.getX(),this.getY()));
				
				updatehelper++;
				if (updatehelper==500){
					for(int k=0; k<3;k++){
						if(mArray[k].getSpec_attribute()==1) mArray[k].update(mArray[k].getNewPoint());
					}
					updatehelper=0;
				}
				for(int i=0; i<wArray.length;i++){
					wArray[i].mapchange();
				}
				
				// szörnyekkel való ütközés vizsgálata, találat esetén alaphelyzetbe állítás és életvesztés
				for(int k=0; k<2; k++){
					for(int m=0; m<3; m++){
						if(mArray[m].getSpec_attribute()!=0){
							if (player[k].getP().x==mArray[m].getP().x && player[k].getP().y==mArray[m].getP().y){
								player[k].removeLife();
							}
						}
					}
				}
				/*player[1]=syncArray[1].toPlayer();
				player[1].bomb[0] = syncArray[4].toBomb();
				player[1].bomb[1] = syncArray[5].toBomb();	*/	
			}		
			else{
				System.out.println("Kliens update");
				player[0]=syncArray[0].toPlayer();
				player[0].bomb[0] = syncArray[2].toBomb();
				player[0].bomb[1] = syncArray[3].toBomb();
				
				// if player1 dies, respawn at (13,13)
				if(player[1].respawn == true){
					player[1].update(new Point(13,13));
					player[1].respawn = false;
				}
				else player[1].update(new Point(this.getX(),this.getY()));
				for(int i=6; i<9;i++){
					mArray[i-6] = GamePanel.syncArray[i].toMonster();
				}
				for(int i=9;i<59;i++){
					wArray[i-9] = GamePanel.syncArray[i].toWall();
					TileMap.map[wArray[i-9].getP().x][wArray[i-9].getP().y] = 2;
				}
			}
		}
		//rajzolófüggvény
		//minden egyes meghívásánál újra rajzolja az objektumokat
		private void render(){	
			tileMap.draw(g); //pálya 
			player[0].draw(g,player[0].idleSprites[0]); //játékos
			player[1].draw(g,player[1].idleSprites[1]); //játékos
			for(int i=0; i<3;i++){
				if(mArray[i].getSpec_attribute()==1) mArray[i].draw(g, mArray[i].idleSprites[0]); //szörny
			}
			//bomba
			if(player[GPID].hasBomb[0]==true){			
				ExplodeAction ea = new ExplodeAction(player[GPID].bomb[0]); 
				new Thread(ea).start(); //"robbantási szál" futtatása
				player[GPID].bomb[0].plantBomb(g, player[GPID]);
				player[GPID].hasBomb[0] = false;
			}
			if(player[GPID].hasBomb[1]==true){			
				ExplodeAction ea = new ExplodeAction(player[GPID].bomb[1]); 
				new Thread(ea).start(); //"robbantási szál" futtatása
				player[GPID].bomb[1].plantBomb(g, player[GPID]);
				player[GPID].hasBomb[1] = false;
			}
			//bombák lerakása
			if(player[GPID].bomb[0].isPlanted()==true) {
				player[GPID].bomb[0].draw(g,player[GPID].bomb[0].idleSprites[0]);
			}
			if(player[GPID].bomb[1].isPlanted()==true) {
				player[GPID].bomb[1].draw(g,player[GPID].bomb[1].idleSprites[0]);
			}
			//falak kirajzolása
			for(int i = 0;i<wArray.length;i++){
				if(wArray[i].getSpec_attribute()==1) wArray[i].drawWall(g);
			}
		}	
		private void draw(){
			Graphics g2 = getGraphics();
			g2.drawImage(image,0,0,null);
			g2.dispose();
		}
		public void initwall(){// falak inicializálása , random geneárottrral
			 for (int i=0;i<50;i++){
				 Point p=new Point(randXY());
				 wArray[i] = new Wall(100+i,p,"Wall"+i,1);
				 TileMap.map[p.x][p.y]=2;
			 }
		}
		public Point randXY(){
			int tx = 0;
			int ty = 0;
			Random r = new Random();
			while( tx == 0 && ty == 0){
				tx = r.nextInt((13 - 1) ) + 1;
				ty = r.nextInt((13 - 1) ) + 1;
				if(((tx % 2) == 0) && ((ty % 2) == 0) || ((tx == 1) && (ty == 1)) || ((tx == 2) && (ty == 1)) || ((tx == 1) && (ty == 2)) ) {
					tx = 0;
					ty = 0;
				}
			}
			return new Point(tx,ty);
		}
		public Point randmonsterXY(){
			Point p = new Point(0,0);
			Random r = new Random();
			while( p.x == 0 && p.y == 0){
				p.x = r.nextInt((13 - 1) ) + 1;
				p.y = r.nextInt((13 - 1) ) + 1;
				if(((p.x % 2) == 0) && ((p.y % 2) == 0) || ((p.x == 1) && (p.y == 1)) || ((p.x == 2) && (p.y == 1)) || ((p.x == 1) && (p.y == 2))|| ((p.x == 1) && (p.y == 3))|| ((p.x == 3) && (p.y == 1)) ) {
					p.x = 0;
					p.y = 0;
				}
				else if(TileMap.map[p.x][p.y] ==2) p.x = p.y = 0;
			}
			return p;
		}
		//ebbe a tömbbe szinkronizálja a kliens és a szerver is az adatait
		//GPID alapján lehet azonosítani, hogy most melyik oldal írja be az adatait, ebből tudja beolvasni a nem az adott GP-hez
		//tartozó adatokat
		//ezt a tömböt küldözgetjük a hálózaton
		public void sync(){
			if(GPID == 0){
				GamePanel.syncArray[0] = player[0].toFO();
				//GamePanel.syncArray[1] = player[1].toFO();
				GamePanel.syncArray[2] = player[0].bomb[0].toFO();
				GamePanel.syncArray[3] = player[0].bomb[1].toFO();
				//GamePanel.syncArray[4] = player[1].bomb[0].toFO();
				//GamePanel.syncArray[5] = player[1].bomb[1].toFO();
				for(int i=6; i<9;i++){
					GamePanel.syncArray[i] = mArray[i-6].toFO();
				}
				for(int i=9;i<59;i++){
					GamePanel.syncArray[i] = wArray[i-9].toFO();
				}
			}
			else{
				//GamePanel.syncArray[0] = player[0].toFO();
				GamePanel.syncArray[1] = player[1].toFO();
				//GamePanel.syncArray[2] = player[0].bomb[0].toFO();
				//GamePanel.syncArray[3] = player[0].bomb[1].toFO();
				GamePanel.syncArray[4] = player[1].bomb[0].toFO();
				GamePanel.syncArray[5] = player[1].bomb[1].toFO();
				/*for(int i=6; i<9;i++){
					mArray[i-6] = GamePanel.syncArray[i].toMonster();
				}
				for(int i=9;i<59;i++){
					wArray[i-9] = GamePanel.syncArray[i].toWall();
					TileMap.map[wArray[i-9].getP().x][wArray[i-9].getP().y] = 2;
				}*/
				
			}
		}
		//setters,getters
		public int getX() {
			return x;
		}

		public void setX(int x) {
			if(x > 13) this.x = 13;
			else if (x < 1 ) this.x = 1;
			else this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			if(y > 13) this.y = 13;
			else if (y < 1 ) this.y = 1;
			else this.y = y;
		}
		public int getGPID() {
			return GPID;
		}

		public void setGPID(int gPID) {
			GPID = gPID;
		}

		public Player[] getPlayer() {
			return player;
		}

		public void setPlayer(Player[] player) {
			this.player = player;
		}
	}
	

