package hu.bme.mit.szofttech.dynablaster;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;


public class Bomb extends FieldObject{

	private static final long serialVersionUID = 4L; //egyéni azonosító
	public BufferedImage [] idleSprites; //képbuffer tömb
	private boolean planted; //megmondja, hogy az adott bomba le van-e rakva
	private boolean exploded; //megmondja, hogy az adott bomba felrobbant-e, ha igen, akkor a hatósugárban "törölni" kell az elemeket
	
	public Bomb(int iD, Point p, String name,int r) {
		super(iD, p, name, r);
		idleSprites = new BufferedImage[1]; //egy elemű, mivel csak 1 bomba skin van
		try{
			idleSprites[0] = ImageIO.read(new File("resources/bomb.png")); //beolvasás
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		this.exploded = false;
	}
	//setters,getters
	public boolean isPlanted() {
		return planted;
	}

	public void setPlanted(boolean planted) {
		this.planted = planted;
	}
	public boolean isExploded() {
		return exploded;
	}
	public void setExploded(boolean exploded) {
		this.exploded = exploded;
	}
	//bomba lerakása, a koordináták megjegyzéséhez szükséges a játékos átvétele
	public void plantBomb(Graphics2D g, Player p){
		int tx= p.getP().x;
		int ty= p.getP().y;
		TileMap.map[tx][ty] = 2;	// bomba átjárhatósága
		GamePanel.BombPoints[this.getID()-10] = new Point(tx,ty);
		this.setP(new Point(tx,ty));
		this.setPlanted(true);
		draw(g,this.idleSprites[0]);
	}
	//bomba robbantás hatása
	//TODO: működésre bírni
	public void explosion(){
		if(isExploded()==true){
			Point p0 = new Point(super.getP());
			System.out.println("Explosion point: "+p0);
			int rad0 = super.getSpec_attribute();
			//végignézi a 4 lehetséges irányban a mezőket és 1-be állítja azokat
			//ha közben fix falat talál, többet nem nézi arra
			Boolean[]fal = new Boolean[4];
			for (int i=0; i<4;i++){
				fal[i]=true;
			}
			for(int i=0;i<rad0;i++){
				if(p0.x-i>0 && fal[0]){
					if(TileMap.map[p0.x-i][p0.y]==0) fal[0]=false;
					else {
						TileMap.map[p0.x-i][p0.y] = 1;
						for(int j=0;j<3;j++){
							if(GamePanel.mArray[j].getP().x == p0.x-i && GamePanel.mArray[j].getP().y == p0.y) GamePanel.mArray[j].setSpec_attribute(0); // ellenőrzi, hogy van-e adott irányban szörny
						}
						for(int j=0;j<2;j++){
							if(GamePanel.player[j].getP().x == p0.x-i && GamePanel.player[j].getP().y == p0.y) GamePanel.player[j].removeLife();
						}
					}
				}
				if(p0.x+i<14 && fal[1])
					if(TileMap.map[p0.x+i][p0.y]==0) fal[1]=false;
					else {
						TileMap.map[p0.x+i][p0.y] = 1;
						for(int j=0;j<3;j++){
							if(GamePanel.mArray[j].getP().x == p0.x+i && GamePanel.mArray[j].getP().y == p0.y) GamePanel.mArray[j].setSpec_attribute(0);
						}
						for(int j=0;j<2;j++){
							if(GamePanel.player[j].getP().x == p0.x+i && GamePanel.player[j].getP().y == p0.y) GamePanel.player[j].removeLife();
						}
					}
				if(p0.y-i>0 && fal[2])
					if(TileMap.map[p0.x][p0.y-i]==0) fal[2]=false;
					else {
						TileMap.map[p0.x][p0.y-i] = 1;
						for(int j=0;j<3;j++){
							if(GamePanel.mArray[j].getP().y == p0.y-i && GamePanel.mArray[j].getP().x == p0.x) GamePanel.mArray[j].setSpec_attribute(0);
						}
						for(int j=0;j<2;j++){
							if(GamePanel.player[j].getP().y == p0.y-i && GamePanel.player[j].getP().x == p0.x) GamePanel.player[j].removeLife();
						}
					}
				if(p0.y+i<14 && fal[3])
					if(TileMap.map[p0.x][p0.y+i]==0) fal[3]=false;
					else {
						TileMap.map[p0.x][p0.y+i] = 1;
						for(int j=0;j<3;j++){
							//if(GamePanel.mArray[j].getP()==new Point(p0.x-i,p0.y)) GamePanel.mArray[j].setSpec_attribute(0);
							if(GamePanel.mArray[j].getP().y == p0.y+i && GamePanel.mArray[j].getP().x == p0.x) GamePanel.mArray[j].setSpec_attribute(0);
						}
						for(int j=0;j<2;j++){
							if(GamePanel.player[j].getP().y == p0.y+i && GamePanel.player[j].getP().x == p0.x) GamePanel.player[j].removeLife();
						}
						
					}
			}
			TileMap.map[p0.x][p0.y] = 1;//a bomba helyének 1-be állítása arra az esetre, ha szörny lépne rá 
		}
		setExploded(false);
	}
	public FieldObject toFO(){
		FieldObject tmpFO = new FieldObject(this.getID(),this.getP(),this.getName(),super.getSpec_attribute());
		return tmpFO;
	}
}
