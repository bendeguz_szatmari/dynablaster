package hu.bme.mit.szofttech.dynablaster;

import java.io.*;
import java.util.Random;
import java.awt.*;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class TileMap {
	public BufferedImage [] idleSprites;
	private int x;
	private int y;
	
	private int tileSize;
	public static int [][] map=null;
	
	protected int helpmobilemap=0;
	protected int []mobilemapx;
	protected int []mobilemapy;
	
	public int mapWidth;
	public int mapHeight;
	
	
	public TileMap(String s, int tileSize){
		
		this.tileSize = tileSize;			//egy egys�g m�ret magyar�n a n�gyzet
		
		
		idleSprites = new BufferedImage[1];
		try{
			idleSprites[0] = ImageIO.read(new File("resources/wall_stat.png"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
		
		
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(s));
			
			mapWidth = Integer.parseInt(br.readLine());
			mapHeight = Integer.parseInt(br.readLine());
			map = new int [mapHeight][mapWidth]; 
			
			String delimiters = " ";
			for ( int row =0; row < mapHeight;row++){
				String line = br.readLine();
				String[] tokens =line.split(delimiters);
				for(int col = 0; col < mapWidth; col ++){
					map[row][col]= Integer.parseInt(tokens[col]);
				}
			}
		}
		catch ( Exception e) {}
	}
	
	public int getx() {return x;}
	public int gety() {return y;}
	
	public int getColTile(int x ){		//x koordin�ta megkap�sa
		return x ;
	}
	
	public int getRowTile(int y){ 		//y koordin�ta megkap�sa
		return y;	
	}
	public int getTile(int row,int col){
		return  map[row][col];
		
	}
	public void setx(int i) {x=i;}
	public void sety(int i) {y=i;}
	
	public int [][] getwall()
	{
		return map;
	}
	public static int[][] cloneArray() {			//az eredeti pálya lemásolása
	    int length = map.length;
	    int[][] src = new int[length][map[0].length];
	    for (int i = 0; i < length; i++) {
	        System.arraycopy(map[i], 0, src[i], 0, map[i].length);
	    }
	    return src;
	}
	
	public int mapvalue(int x,int y){			// adott helyen a térkép értékének lekérdezése
		return map[getx()][gety()];
	}
	
	public void draw(Graphics2D g){					//p�lyakirajzol�s van egy border �s egy p�lya r�sz 
													//p�lyar�sz 2-14 koordin�t�ig van mind x �s mind y ir�nyban
													//bal fenti koordin�ta az 1;1
		for ( int row= 0; row < mapHeight; 	row++){
			for ( int col = 0; col < mapWidth; col++)
			{
				int rc = map[col][row];
				
				if ( rc == 0 ){							//biztos torlasz

					g.drawImage(
							this.idleSprites[0],
							(this.x+col*30),
							(this.y+row*30),
							null
							);	
				}
				if( rc == 1 || rc == 2 || rc == 3){							//út ahol lehet közlekedni
					g.setColor(Color.getHSBColor(10, 20, 160));
					g.fillRect(x+col*tileSize,y+row*tileSize, tileSize, tileSize);
				}
				//

			}
		}		
		
	}
}
