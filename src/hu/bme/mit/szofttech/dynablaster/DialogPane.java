package hu.bme.mit.szofttech.dynablaster;

import java.awt.GridLayout;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.*;

class DialogPane {
	//kezdeti párbeszédablak a kliens és a szerver futtatásához
	public static String IP = null;
	private String type = null;
	
    public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public DialogPane() {
        JPanel panel = new JPanel(new GridLayout(0, 1));
        String[] choices = { "Server","Client" };
        String selection = (String) JOptionPane.showInputDialog(null, null,
            "Choose platform", JOptionPane.QUESTION_MESSAGE, null,
            choices, // Array of choices
            choices[0]); // Initial choice
        	System.out.println("Selection: " + selection);   
        if(selection.matches("Client")){
	        String address = JOptionPane.showInputDialog(
	            panel,
	            "Form: xxx.xxx.xxx.xxx for example: 127.0.0.1", 
	            "Server select by IP address", 
	            JOptionPane.PLAIN_MESSAGE
	        );
	        //reguláris kifejezés a helyes IP cím szintaktika ellenőrzésére
	        String REGEX = "\\d{1,3}[.]\\d{1,3}[.]\\d{1,3}[.]\\d{1,3}";
	        try{
	            if(!address.matches(REGEX)){
	            	throw new Exception();        	
	            }
	            System.out.println("Server IP address: " + address); 
		        this.setType("Client");
	        }
	        catch(Exception e){
	        	System.err.println("Invalid IP address");
	        	System.err.println("System is exiting");
	        	System.exit(0);
	        }
        }
        else {
    		InetAddress host_ip = null;
    		try {
    			host_ip = InetAddress.getLocalHost();
    			System.out.println("IP address of the host: "+host_ip.getHostAddress());
    			IP = "localhost"; //teszt
    			//IP = host_ip.getHostAddress(); //végső használatban ez lesz
    			this.setType("Server");
    		} catch (UnknownHostException e) {
    			System.err.println("Couldn't get IP address of the host");
    			e.printStackTrace();
    			System.exit(1);
    		}        	
        }

    }
}