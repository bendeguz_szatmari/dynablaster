package hu.bme.mit.szofttech.dynablaster;

import java.awt.Point;
import java.awt.image.*;

import javax.imageio.ImageIO;

import java.io.File;

/**
 * Thiss nfghxfghgfh
 * @author Bendi
 *
 */
public class Player extends FieldObject {
	
	private static final long serialVersionUID = 1L;

	public Bomb []bomb = new Bomb[2];
	public boolean []hasBomb = new boolean[2];
	
	public BufferedImage [] idleSprites;
	public boolean respawn = false;

	public Player(int iD, Point p, String name, int hp){
		
		super(iD, p, name, hp);
		hasBomb[0] = false;
		hasBomb[1] = false;
		bomb[0] = new Bomb(10, new Point(1,1), "Bomb0", 3);
		bomb[1] = new Bomb(11, new Point(1,1), "Bomb1", 3);
		try{
			idleSprites = new  BufferedImage[2];
			idleSprites[0]=ImageIO.read (new File("resources/player1.png"));
			idleSprites[1]=ImageIO.read (new File("resources/player2.png"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void update(Point new_position){
		super.setP(new_position);
	}
	
	/**
	 * ghdghdghgh
	 * @return tmpghugdhd 
	 */
	public FieldObject toFO(){
		FieldObject tmpFO = new FieldObject(this.getID(),this.getP(),this.getName(),super.getSpec_attribute());
		return tmpFO;
	}

	public boolean []getHasBomb() {
		return hasBomb;
	}

	public void setHasBomb(boolean hasBomb[]) {
		this.hasBomb = hasBomb;
	}
	
	
	public int getLife(){
		return super.getSpec_attribute();
	}

	public void setLife(int life){
		super.setSpec_attribute(life);
	}
	
	public void removeLife(){
		this.setLife(getLife()-1);
		this.respawn = true;
		System.out.println("Player"+this.getID()+" has lost a life");
	}
}
