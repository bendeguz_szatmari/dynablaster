
package hu.bme.mit.szofttech.dynablaster;

import java.awt.Point;
import java.io.ObjectInputStream;
import java.util.TimerTask;
import java.util.Timer;

class Control implements Runnable {

	private GUI gui;
	private Network net = null;
	private Timer timer = new Timer();
	private FieldObject[][] FieldObjectTable= new FieldObject[13][13];

	Control() {
		for(int i = 0;i<13;i++){
			for(int j = 0; j<13;j++){
				FieldObjectTable[i][j] = new Wall(i+j,new Point(i,j),"Wall" +(i+j),1);
			}
		}
		FieldObjectTable[0][0] = new Player(0,Player.P0,"Player"+0,3);
		FieldObjectTable[12][12] = new Player(1,Player.P1,"Player"+1,3);
		timer.scheduleAtFixedRate(new TimerTask(){

			@Override
			public void run() {
				updateFieldObjectTable();
			}
		}, 0, 1000);
	}

	void setGUI(GUI g) {
		gui = g;
	}
	GUI getGUI(){
		return gui;
	}	
	//szerver indítása
	//további változtatások várhatóak
	void startServer(String ip) {
		if (net != null)
			net.disconnect();
		net = new SerialServer(this);
		net.connect("localhost");	
	}
	//kliens indítása IP alapján
	void startClient(String ip) {
		if (net != null)
			net.disconnect();
		net = new SerialClient(this);
		net.connect(ip);
	}
	//adat küldése a hálón
	public void sendData(FieldObject[] fo) {
		if (net == null)
			return;
		net.send(fo);
	}
	//adat fogadása a soros bájtfolyamból
	public void receiveData(ObjectInputStream input) {
		if (gui == null)
			return;
		else {
			try{
				GamePanel.syncArray =(FieldObject[]) input.readObject();
				System.out.println("Data received");
			}
			
			catch(Exception e){
				System.out.println(e.getMessage());
				System.err.println("Object receive problem");
				System.exit(1);
			}
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	/**
	 * Updates the FieldObjectTable based on onCommand() function
	 */
	public void updateFieldObjectTable(){
		FieldObjectTable[0][0].print();
		FieldObjectTable[12][12].print();
	}
}
