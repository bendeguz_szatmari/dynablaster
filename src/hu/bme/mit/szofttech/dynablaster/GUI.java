
package hu.bme.mit.szofttech.dynablaster;

import javax.swing.JFrame;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private Control ctrl;
	private GamePanel gp;

	GamePanel getGP()
	{
		return this.gp;
	}
	GUI(Control c) {



		if(dialog().matches("Server")) {
			gp = new GamePanel(0);
			JFrame window = new JFrame ("Server");//panel name set
			window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			window.setContentPane(gp);
			window.pack();
			ctrl = c;
			ctrl.startServer(DialogPane.IP);
			window.setVisible(true);
		}
		else {
			gp = new GamePanel(1);
			JFrame window = new JFrame ("Client");
			window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			window.setContentPane(gp);
			window.pack();
			ctrl = c;
			ctrl.startClient(DialogPane.IP);
			window.setVisible(true);
		}
		
	}
	//returns what the application's role should be: Server or Client
	public String dialog(){
		DialogPane gpSelect = new DialogPane();
		return gpSelect.getType();
	}
}
