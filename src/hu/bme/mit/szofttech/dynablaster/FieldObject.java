package hu.bme.mit.szofttech.dynablaster;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

//alapvetően a FieldObject leszármazottja minden elem a pályán
//mindenki rendelkezik egy koordinátával, egy ID-val és egy névvel
public class FieldObject implements Serializable{
	
	private static final long serialVersionUID = 0L; //sorosítható objektum, ezért rendelkeznie kell egy azonosítóval
	private int ID;	 //ID azonosításhoz, egyedi
	private Point p; //koordináta, [1:13],[1:13]
	private String name; //név, pl: játékos1, játékos2, bomba, szörny, nem kötelezően egyedi
	private int spec_attribute; //minden FO-nak különböző, pl.: Player - hp, Bomb - lifetime, stb
	private final static int WIDTH = 30; //a pálya szegélyekkel együtt 450x450-es, egy mező 30x30-as ezt adjuk itt meg
	private final static int HEIGHT = 30;
	
	//alap kezdőpontok a játékosoknak, bal fölső és jobb alsó sarok
	public final static Point P0 = new Point(1,1);
	public final static Point P1 = new Point(13,13);
	//private static Point Phelp=tilemap.randmonsterXY(); //ez itt elég nagy baj, hogy a monster pontjai nem lehetnek statikusak mert random generálodnak
	//public final static Point P2 = Phelp;
	

	//konstruktor
	public FieldObject(int iD, Point p, String name, int sa) {
		super();
		this.ID = iD;		
		this.p = p;
		this.name = name;
		this.spec_attribute = sa;	
	}
	//getters, setters
	public Point getP() {
		return p;
	}
	public void setP(Point p) {
		this.p = p;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getSpec_attribute() {
		return spec_attribute;
	}
	public void setSpec_attribute(int spec_attribute) {
		this.spec_attribute = spec_attribute;
	}
	public static int getWidth() {
		return WIDTH;
	}
	public static int getHeight() {
		return HEIGHT;
	}
	public void print(){
		System.out.println("ID:" + this.ID);
		System.out.println("Point: x=" + this.p.x + " y=" + this.p.y);
		System.out.println("Name: " + this.name);
		System.out.println("Special attribute: "+ this.spec_attribute);
	}
	//hálózati kapcsolthoz sorosítás
	void writeObject(ObjectOutputStream out) throws IOException{
		out.writeInt(this.ID);
		out.writeObject(this.p);
		out.writeObject(this.name);
		out.writeObject(this.spec_attribute);
	}
	//hálózati kapcsolathoz kiolvasása sorosított bájtfolyamból
	void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		this.ID = in.readInt();
		this.p = (Point) in.readObject();
		this.name = (String) in.readObject();
		this.spec_attribute = in.readInt();
	}
	//rajzolás koordináták alapján
	void draw(Graphics2D g, BufferedImage bi){
		g.drawImage(
				bi,
				(this.getP().x*WIDTH),
				(this.getP().y*HEIGHT),
				null
				);	
	}
	Player toPlayer (){
		return new Player(this.ID,this.p,this.name,this.spec_attribute);
	}
	Bomb toBomb (){
		return new Bomb(this.ID,this.p,this.name,this.spec_attribute);
	}
	Monster toMonster (){
		return new Monster(this.ID,this.p,this.name,this.spec_attribute);
	}
	Wall toWall (){
		return new Wall(this.ID,this.p,this.name,this.spec_attribute);
	}
}
