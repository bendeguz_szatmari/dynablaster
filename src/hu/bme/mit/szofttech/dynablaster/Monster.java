package hu.bme.mit.szofttech.dynablaster;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class Monster extends FieldObject {
	public BufferedImage [] idleSprites;
	private int direction = getdirection();	
	private int d = 0;//1=jobb,2=bal,3=fel,4=le az irányok és a nekik megfelelő érték 
	//Point p;
	
	
	
	private static final long serialVersionUID = 2L;

	public Monster(int iD, Point p, String name, int hp) {
		super(iD, p, name,hp);
		idleSprites = new  BufferedImage[1];
		try {
			idleSprites[0]=ImageIO.read (new File("resources/monster.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Point getPoint(Point p){		// lekérdezzük a pont koordinátáit lehet nem is kell
		return new Point (p.x,p.y);
	}
	
	public Point getNewPoint(){
		int x = 0;
		int y = 0;
		boolean kilép = false;
		int  olddirection = direction; 		//alapirány
		int noDirection = 0; //blokkolt irányok számával egyezik meg, ha minden oldalról körbe van véve a szörny, akkor az nem mozdul

		x=(int) super.getP().x;					
		y=(int) super.getP().y;
		while(kilép==false){
		if(d==1 || olddirection==1){							//jobbra lépés vizsgálata
			if(TileMap.map[x+1][y]==1){
				x++;
				kilép = true;
			}
			else noDirection++;
		}
		if(d==2 || olddirection==2){							//balra lépés vizsgálata
			if(TileMap.map[x-1][y]==1){
				x--;
				kilép = true;
			}
			else noDirection++;
		}
		if(d==3 || olddirection==3){							//felfele lépés vizsgálata
			if(TileMap.map[x][y-1]==1){
				y--;
				kilép = true;
			}
			else noDirection++;
		}
		if(d==4 || olddirection==4){							//lefele lépés vizsgálata
			if(TileMap.map[x][y+1]==1){
				y++;
				kilép = true;
			}
			else noDirection++;
		}
		if(noDirection == 4) return new Point (x,y);
		if(kilép==false) {d=getdirection(); direction = 0;}
		}
		return new Point (x,y);
	}

	
	public void update(Point new_position){
			super.setP(new_position);
	}
	
	public FieldObject toFO(){
		FieldObject tmpFO = new FieldObject(this.getID(),this.getP(),this.getName(),super.getSpec_attribute());
		return tmpFO;
	}
	
	
	private int getdirection(){
	Random r = new Random();
	int Low = 1;
	int High = 5;
	int R = r.nextInt(High-Low) + Low;
	return R;
	}
	
}
