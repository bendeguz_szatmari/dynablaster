package hu.bme.mit.szofttech.dynablaster;

abstract class Network {

	public Control ctrl;

	Network(Control c) {
		ctrl = c;
	}

	abstract void connect(String ip);

	abstract void disconnect();

	abstract void send(FieldObject[] fo);
}
