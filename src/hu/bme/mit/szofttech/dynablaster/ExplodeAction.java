package hu.bme.mit.szofttech.dynablaster;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;
//robbanás végrehajtására
public final class ExplodeAction implements Runnable{

	private Bomb bomb;
	private int delay;
	private volatile Timer t;
	//konstruktor, a bomba 3s-ig "él"
	public ExplodeAction(Bomb b){
		bomb = b;
		delay = 3000;
	}
	//setters,getters
	public Bomb getBomb() {
		return bomb;
	}

	public void setBomb(Bomb bomb) {
		this.bomb = bomb;
	}

	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	//amikor lerakjuk a bombát ezt a szálat kezdjük el futtatni
	//3s után egy eseményt generál, ami a bombát kezeli
	@Override
	public void run() {
		this.t = new Timer(delay, taskPerformer);
		if(t.isRunning()) t.stop();
		t.start();
	}
	ActionListener taskPerformer = new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
	    	bomb.setPlanted(false);
	    	while(t.isRunning()){
				try {
					t.stop();
					bomb.setExploded(true);
					System.out.println(bomb.getName() + " is exploded: "+!t.isRunning());
					bomb.explosion();
				} catch (Exception e) {
					e.printStackTrace();
				}
	    	}
	    }
	};
}
