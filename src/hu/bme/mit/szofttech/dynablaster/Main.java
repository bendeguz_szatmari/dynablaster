package hu.bme.mit.szofttech.dynablaster;

public class Main {
	public static void main(String[] args)  {	
		Control c = new Control();
		GUI g = new GUI(c);
		c.setGUI(g);
	}
}
