package hu.bme.mit.szofttech.dynablaster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public class MotionAction extends AbstractAction implements ActionListener 
	{

		private static final long serialVersionUID = 6L;
		private int deltaX;
		private int deltaY;
		private GamePanel gp;
		private TileMap tilemap;
		int boolwall;
		int [][]helpmap;

		public MotionAction(String name, int deltaX, int deltaY,GamePanel gp)
		{
			super(name);

			this.deltaX = deltaX;
			this.deltaY = deltaY;
			this.gp = gp;
		}
		
		public MotionAction(GamePanel gp){
			this.gp = gp;
		}

		//called on buttonpress
		public void actionPerformed(ActionEvent e)
		{
			System.out.println("Button action performed");
			if(Math.abs(deltaX) == 1 || Math.abs(deltaY) == 1){
				//local variable for deltas
				int dx = deltaX;
				int dy = deltaY;
				
				helpmap=TileMap.cloneArray();								// pálya lemásolása
				if (helpmap[gp.getX()+dx] [gp.getY()+dy]==2 || helpmap[gp.getX()+dx] [gp.getY()+dy]==3){ dx=0;dy=0;}	//ha falba ütköznénk 		x-y konverzió probléma vanjól működik de fel van cserélve x és y valahol
				//check of the next coordinate, if it indicates forbidden place no movement is made
				if(((gp.getY() % 2) == 0) && (((gp.getX()+dx) % 2) == 0)) dx = 0;
				if(((gp.getX() % 2) == 0) && (((gp.getY()+dy) % 2) == 0)) dy = 0;

				gp.setX(gp.getX()+dx);
				gp.setY(gp.getY()+dy);
				//current value print
				//System.out.println("Action performed. Player's new position: x=" + gp.getX() + " y=" + gp.getY());
			}
			else if(gp.player[0].bomb[0].isPlanted()==false){
				gp.player[0].hasBomb[0] = true;
				System.out.println("Bomb0 plant");
				deltaX = deltaY = 0;
			}
			else if(gp.player[0].bomb[1].isPlanted()==false){
				gp.player[0].hasBomb[1] = true;
				System.out.println("Bomb1 plant");
				deltaX = deltaY = 0;
			}
			
		}

		public MotionAction addAction(String name, int deltaX, int deltaY, GamePanel gp)
		{
			MotionAction action = new MotionAction(name, deltaX, deltaY,gp);

			KeyStroke pressedKeyStroke = KeyStroke.getKeyStroke(name);
			InputMap inputMap = gp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
			inputMap.put(pressedKeyStroke, name);
			gp.getActionMap().put(name, action);

			return action;
		}

		public void addMotionSupport()
		{
			int delta = 1;
			addAction("LEFT", -delta,  0, gp);
			addAction("RIGHT", delta,  0, gp);
			addAction("UP",    0, -delta, gp);
			addAction("DOWN",  0,  delta, gp);
			addAction("SPACE", 99,99, gp);
			
			System.out.println("Keymotion support added");
		}
	}
