package hu.bme.mit.szofttech.dynablaster;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class Wall extends FieldObject {
	public BufferedImage [] idleSprites;
	private static final long serialVersionUID = 5L;

	public Wall(int iD, Point p, String name, int hp) {
		super(iD, p, name, hp);
		idleSprites = new BufferedImage[1];
		try{
			idleSprites[0] = ImageIO.read(new File("resources/wall.png"));
		}
		catch(Exception e) {
			e.printStackTrace();
		}		 
	}
	//fal kirajzolás
	public void drawWall(Graphics2D g){
		int tx= super.getP().x;
		int ty= super.getP().y;
		this.setP(new Point(tx,ty));
		draw(g,this.idleSprites[0]);
	}
	//amikor a bomba miatt megváltozik az alakja, 0-ba kell állítani a spec_Attribute-t, hogy eltűnjön a fal
	//ekkor a map tömb megfelelő értékét 1-be kell állítani
	public void mapchange(){			
		if(super.getSpec_attribute()==0) TileMap.map[super.getP().x][super.getP().y]=1;
		if(TileMap.map[super.getP().x][super.getP().y]==1) super.setSpec_attribute(0);
	}
	public FieldObject toFO(){
		FieldObject tmpFO = new FieldObject(this.getID(),this.getP(),this.getName(),super.getSpec_attribute());
		return tmpFO;
	}
}
